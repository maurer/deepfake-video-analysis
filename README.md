# deepfake-video-analysis
A lightweight Neural Network to extract pilot information from face swap videos.

![real video](./figs/real.gif)      ![fake video](./figs/fake.gif)

## Repository structure
- `checkpoints/` : the checkpoints for the pre-trained FAb-Net encoder and the Siamese Network.
- `ds/` : the dataset generated for this project.
- `figs/` : some README medias.
- `gen_deepfakes/` : all the scripts and Jupyter notebooks we used to generate the deepfakes and create the dataset.
- `scripts/` : all the scripts and Jupyter notebooks for the neural network pipeline.

To know more about the dataset and the deepfake generation process, look at the respective READMEs or read the project's [report](report.pdf).

## Reproduce the experiments
All the experiments were run on [Google Colab](https://colab.research.google.com/), using T4-GPU acceleration.

### 0 | Crop the videos
First of all, all videos (both `real/` and `fake/`) must be cropped around the face. This can be done by running the notebook [`crop-video`](scripts/0_crop-video.ipynb) twice, properly setting the `DATA_DIR` variable.

### 1 | Prepare the subsets
Then, the dataset must be split into training, validation and testing sets. To do so, run the notebook [`prepare-dataset`](scripts/1_prepare-dataset.ipynb).

### 2 | Train the network
Finally, the network can be trained by running the notebook [`CNN-siamese-pipeline`](scripts/2_CNN-video-siamese-pipeline.ipynb). The training implements an automatic checkpoint mechanism. Once the checkpoint is loaded, the training phase can be skipped and validation and testing can be directly done.
