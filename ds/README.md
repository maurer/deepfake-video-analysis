# Dataset
This folder contains the dataset we created for this project.

> ⚠️ Unfortunately, these files are too big to be pushed to GitLab. They will be provided separately.

![Some samples from the dataset](../figs/ds_pics.png)

To generate the folders, just run:
```
tar -zxf real.tgz
tar -zxf fake.tgz
tar -zxf frames.tgz
```

The dataset is organized as follows:
- `real/` : the real videos, taken from [Celeb-DF](https://github.com/yuezunli/celeb-deepfakeforensics).
- `frames/` : the best frame for each video, picked using the [SimSwap face score feature](../gen_deepfakes/best_face_from_video.ipynb).
- `fake/` : the face swaps we generated.

Please consult [`dataset.csv`](./dataset.csv) for further information.

Cropped versions of these videos are not available here. They can be generated using the apposite [script](../scripts/0_crop-video.ipynb).

![the dataset distribution](../figs/dataset.svg)