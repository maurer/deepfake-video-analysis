# Generate Deepfakes
This folder contains all the scripts we used to generate the deepfakes.

- [`pull`](pull.ipynb) is used to generate the list of deepfakes to be created. Specifically, these deepfakes are saved in `combinations.csv` and `cross_combinations.csv`.
- [`best-face`](best_face_from_video.ipynb) is used to extract the best frame in a video by using the SimSwap face score method.
- [`generate-df-simswap`](generate_deepfakes_simswap.ipynb) and [`generate-df-roop`](generate_deepfakes_roop.ipynb) are used to generate the deepfakes, starting from the csv files we created before.

---

Original repositories:
- [SimSwap](https://github.com/neuralchen/SimSwap)
- [ROOP](https://github.com/s0md3v/roop)